
from pyecharts import Pie
import csv

def GetSexListOf(fileName):
    listSex = []
    with open(fileName, 'r') as file:
        reader = csv.reader()
        for i in reader:
            listSex.append(i[24])
    return listSex

def VisualSexPyecharts(listSex):
    MAN = "1"
    WOMEN = "2"

    sexDict = dict()
    for currentSex in listSex[1:]:
        if currentSex == MAN:
           sexDict["man"] = sexDict.get("man", 0) + 1;
        elif currentSex == WOMEN:
           sexDict["women"] = sexDict.get("women", 0) + 1;
        else:
           sexDict["unknown"] = sexDict.get("unknown", 0) + 1;

    total = len(listSex)

    attr = ["男性好友","女性好友","未知"]
    value = [sexDict["man"],sexDict["women"],sexDict["unknown"]]

    pie = Pie("好友性别比例","好友总数:%d"%total,title_pos="center")
    pie.add('',attr,value,radius=[30,75],rosetype="area",is_label_show=True)
    pie.show_config()
    pie.render()

# dome
theTestList = ["1","2","1","1","2","1","0"]
VisualSexPyecharts(theTestList)