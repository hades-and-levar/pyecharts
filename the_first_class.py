'''
author：wudi
func: 统计微信好友性别，并可视化
'''

from pyecharts import Pie 
import csv

# 读取csv文件，把性别信息读取出来
def getSex(filename):
    listsex = []
    with open(filename, 'r') as sexfile:
        reader = csv.reader(sexfile)
        for i in reader:
            #print(i[4])
            listsex.append(i[4])
    return listsex

# 性别pyecharts可视化
man = '1'
women = '2' 
def visualSexPyecharts(listsex):
    dictSex = dict()
    for sex in listsex[1:]:
        if sex == man:
            dictSex['man'] = dictSex.get('man', 0) + 1
        elif sex == women:
            dictSex['women'] = dictSex.get('women', 0) + 1
        else:
            dictSex['unknown'] = dictSex.get('unknown', 0) + 1
        
        total = len(listsex[1:])
        
        attr = ['男性','女性','未知']
        value = [dictSex['man'],dictSex['man'],dictSex['man']]
        
        pie = Pie('好友性别比例','好友总数: %d'%total, title_pos='center')
        pie.add('',attr,value,radius=[30,75],rosetype='area',is_label_show=True)
        pie.show_config()
        pie.render()
the_test_list = ['1','1','1','2','2']
sexFile = 'D:\\wudiwechatdata.csv'
listSex = getSex(sexFile)
visualSexPyecharts(listSex)
